node default {
  include kiosk::chrome
}

package { [ 'tint2' ]:
  ensure => 'installed'
}


# network manager

package { ['network-manager']:
  ensure => 'installed'
}


#disable services that do not need to be running
#service { "sshd":
#  enable => false,
#}


#remove network interface
network::interface { 'eth0': ensure => 'absent' }



    exec { 'grub2 remove quiet':
        path    => '/bin:/usr/bin',
        command => "sed -i '/^GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash\"/s/quiet splash//' /etc/default/grub",
        onlyif  => "grep -q '^GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash\"' /etc/default/grub",
        notify  => Exec['update-grub'],
    }

   exec { 'update-grub':
        refreshonly => true,
        path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    }


# Purge Packages we dont need
package { ['modemmanager']:
  ensure => 'purged'
}

package { ['ppp', 'openssh-server']:
  ensure => 'purged'
}

# Office
package { 'libreoffice':
  ensure  => installed
}

# System Monitoring
package { 'conky':
  ensure => installed
}

package { ['xfce4-power-manager', 'xfce4-volumed', 'xfce4-notifyd']:
  ensure => installed
}

    exec { 'pam remove motd1':
        path    => '/bin:/usr/bin',
	command => "sed -i 's,session    optional   pam_motd.so  motd=/run/motd.dynamic noupdate,,' /etc/pam.d/login",
	onlyif  => "grep -q 'pam_motd.so' /etc/pam.d/login",
    }
    exec { 'pam remove motd2':
        path    => '/bin:/usr/bin',
	command => "sed -i 's/session    optional   pam_motd.so//' /etc/pam.d/login",
	onlyif  => "grep -q 'pam_motd.so' /etc/pam.d/login",
    }
    exec { 'pam remove lastlog':
        path    => '/bin:/usr/bin',
	command => "sed -i 's/session    optional   pam_lastlog.so//' /etc/pam.d/login",
	onlyif  => "grep -q 'pam_lastlog.so' /etc/pam.d/login",
    }
    exec { 'run remove motd':
        path    => '/bin:/usr/bin',
	command => "sed -i 's,[ -d \"/etc/update-motd.d\" ] && run-parts --lsbsysinit /etc/update-motd.d > /run/motd.dynamic &,,' /etc/init/mounted-run.conf",
	onlyif  => "grep -q ' [ -d \"/etc/update-motd' /etc/init/mounted-run.conf",
    }
# rm /etc/init/plymouth*
# rm tty2-6