#!/bin/bash
#kind: finish
#name: Ubuntu ec2 Finish

environment=1402production
hostname=$(hostname -f)

if [ "$hostname" = "" ]
then
  hostname=$(hostname)
  echo "Can not resolve fqdn, please enter fqdn of host: (example: $hostname.internal.domain)"
  read hostname
fi

if [ -f /usr/bin/dpkg ]
then
  wget http://apt.puppetlabs.com/puppetlabs-release-stable.deb
  dpkg -i puppetlabs-release-stable.deb
  apt-get --yes --quiet update
  apt-get --yes -o Dpkg::Options::="--force-confold" --quiet install git puppet-common puppet libaugeas-ruby
fi

cat << EOF > /etc/puppet/puppet.conf
#kind: snippet
#name: puppet.conf
[main]
vardir = /var/lib/puppet
logdir = /var/log/puppet
rundir = /var/run/puppet
ssldir = /var/lib/puppet/ssl

EOF

sed -i "s/^certname        = dummyhostname/certname        = $hostname/" /etc/puppet/puppet.conf
sed -i "s/production/$environment/" /etc/puppet/puppet.conf

git clone https://bitbucket.org/minimoo/linuxbuild.git

puppet module install puppetlabs-stdlib
puppet module install ajjahn/network

cd linuxbuild
cd puppet
puppet module build puppet-kiosk
puppet module install puppet-kiosk/pkg/naturalis-kiosk-0.1.0.tar.gz
puppet apply manifests/default.pp

#if [ -f /usr/bin/dpkg ]
#then
#  /bin/sed -i 's/^START=no/START=yes/' /etc/default/puppet
#fi

#/bin/touch /etc/puppet/namespaceauth.conf
#/usr/bin/puppet agent --enable
#/usr/bin/puppet agent --config /etc/puppet/puppet.conf
#/etc/init.d/puppet restart
